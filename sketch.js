var width = window.innerWidth
var height = window.innerHeight


// euclidean distance
let dist = ((p1, p2) => sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2))

// move a point or a triangle by v
let move_thing = (xys, v) => xys.map((e, i) => e + (i%2==0 ? v[0] : v[1]))

// rotate a point or a triangle around point c by theta
function rotate_thing(xys, c, th) {
  let at_origin = move_thing(xys, [-c[0], -c[1]])
  let rotated = at_origin.map((e, i, l) =>
    i%2 == 0
    ? e * cos(th) - l[i+1] * sin(th)  // xs
    : e * cos(th) + l[i-1] * sin(th)  // ys
  )
  return move_thing(rotated, c)
}

// ...I miss python
let range = ((n) => [...Array(n).keys()])

// middle point of a line segment
let segment_midpoint = ((p1, p2) => [(p1[0] + p2[0])/2, (p1[1] + p2[1])/2])


function drawCircle(p) {
  noFill()
  stroke(85, 48, 180)
  strokeWeight(1)
  circle(p[0], p[1], 3)
}

function windowResized() {
  width = windowWidth
  height = windowHeight
  resizeCanvas(width, height)
}

function setup() {
  createCanvas(windowWidth, windowHeight)
  colorMode(HSB, 255)
  frameRate(1)
}

let sinc = x => Math.sin(x) / x

function draw() {
  let bg = color(85, 48, 80)
  background(bg)

  // let threshold = a => (sinc(a * 0.02) * 0.5) + 0.5
  let threshold = a => sinc(a * 0.05) + 0.3
  let sinThreshold = a => (Math.cos(a * 0.024) * 0.5) + 0.5

  for (var y = 0; y < height; y += 10) {
    let thrY = threshold(y - (height / 2))
    for (var x = 0; x < width; x += 10) {
      let circleR = min([width, height]) / 4
      let inCircle = dist([x, y], [width / 2, height /2]) < circleR

      if (inCircle && Math.random() < thrY) {
        drawCircle([x, y])
      }
      
      let thrX = sinThreshold(x - (width / 2))
      if (!inCircle && Math.random() < thrX) {
        drawCircle([x, y])
      }
    }
  }

  drawCircle([50, 50])
}
